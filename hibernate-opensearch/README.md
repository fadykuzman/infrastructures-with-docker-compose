## Important

For Opensearch to work, you need to change the following kernel parameter on the *host* machine:
`vm.max_map_count`
Open (or create) the file /etc/sysctl.conf .
Change the value to 262144

```
vm.max_map_count=262144
```

Then run the following command in the terminal

```
$ sysctl -p
```

To check what the set value is, run the following command:
```
cat /proc/sys/vm/max_map_count
```

[For Opensearch docs](https://opensearch.org/docs/latest/install-and-configure/install-opensearch/index/#important-settings)
